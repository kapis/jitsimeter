# Jitsimeter
Genera un sitio web para comparar instancias la  velocidad y seguridad de diferentes instancias de Jitsi. Permite responder la pregunta: ¿Qué instancia de Jitsi me conviene usar?

Versión pública: https://ladatano.partidopirata.com.ar/jitsimeter/

Licencia: GPLv3

![Captura de pantalla de Jitsimeter](screenshot.png)

## Introducción

Jitsi es una sistema de videoconferencias de software libre que se puede instalar en muchos servidores, por lo que es normal que, aunque se vean iguales, los diferentes servidores ofrezcan diferentes prestaciones.

Jitsimeter permite a cada usuario que entra pueda evaluar más de 200 instancias de Jitsi para saber cuál es la más rápida y la más segura para utilizar. El test tarda 4 minutos y consume datos, deshabilitar otras descargas durante la prueba. Luego de la prueba elegir la instancia con mejor descarga y velocidad, se puede acceder directamente desde el enlace, no requiere registro.

### Mediciones
Lo que esta herramienta intenta hacer es medir las cualidades de la infraestructura que alberga cada uno de los servidores de jitsi, tanto si protegen tu privacidad como si te van a brindar una buena experiencia de llamadas porque son servidores rápidos.

- *Descarga*: Realiza una solicitud GET a un archivo .wav que está presente en todas las instancias de Jitsi, y mide el tiempo que tarda en cargar, por lo que, sabiendo el tamaño del archivo, podemos estimar la velocidad de la conexión. Esta prueba está basada en [jqspeedtest](https://github.com/skycube/jqspeedtest).
- *Respuesta*: Realiza una solicitud HEAD y mide el tiempo que tarda en responder. Esta prueba está basada en [jqspeedtest](https://github.com/skycube/jqspeedtest).
- *Privacidad STUN*: Para encontrar a otros usuarios de jitsi hay que usar lo que se llama un servidor STUN, y muchas instancias de jitsi usan el servidor que provee google, que es gratuito y abierto, pero te deja expuesto a que google sepa que estás iniciando una conversación, por lo que se prefieren otros servidores. Esta prueba está basada en [jitsi-list-generator](https://git.jugendhacker.de/j.r/jitsi-list-generator/).
- *Privacidad Host*: Si el sitio está alojado por google, amazon, cloudflare, o microsoft, se considera que estas empresas abusan de la información que reciben para correlacionarla con otra que tienen, y que incluso se la podrían entregar a los servicios de inteligencia, por lo tanto no se recomienda utilizarlos como proveedor de alojamiento web. Esta prueba está basada en [jitsi-list-generator](https://git.jugendhacker.de/j.r/jitsi-list-generator/).

*Notas:*
- Todas las instancias de la lista de instancias aparecen siempre en el sitio, pero algunas vienen con "available: false", esto significa que jitsi-list-generator no se pudo conectar, aunque esta información no aparece en el listado e igual se realizan las pruebas.

- Hay muchos servidores que rechazan la conexión, porque no están disponibles o porque bloquean las solicitudes entre sitios, aunque esto se hace con el objetivo de limitar los ataques de tipo CORS, perjudica el funcionamiento de Jitsimeter. Los servidores que fallan aparecen con una cruz en las columnas de velocidad y respuesta.

## Armar tu propia instancia de este sitio
Este sitio tiene licencia GPLv3, lo que significa que puedes copiarlo y modificarlo solamente manteniendo la misma licencia y atribuyendo la autoría del código.

Parar armar tu propia instancia sólo es necesario que tengas git para descargar los archivos, y acceso a un servidor web para publicarlos. Si quieres hacer tu propia lista también necesitarás python.

## Pasos
- Clonar jitsimeter: ```git clone https://0xacab.org/faras/jitsimeter/```
- Clonar jitsi-list-generator: ```git clone https://git.jugendhacker.de/j.r/jitsi-list-generator/```
- Seguir las instrucciones de instalación de jitsi-list-generator aquí: https://git.jugendhacker.de/j.r/jitsi-list-generator/src/branch/feature-json-output#user-content-install
- Acceder a la carpeta de jitsi-list-generador: ```cd jitsi-list-generator```
- Si quieres actualizar la lista de instancias de fastarmafia: ```curl https://codeberg.org/favstarmafia/Jitsi_Server_Liste/raw/branch/master/JitsiInstanzen.txt > ../jitsimeter/instances.txt```
- Si quieres agregar tu propia instancia, debes editar el archivo instances.txt, ej: ```echo '"[DOMINIO]",' > instances.txt```.
- Activar el entorno: ```source env/bin/activate```
- Generar lista: ```python3 main.py ../jitsimeter/instances.txt -f json > ../jitsimeter/instances.json```

Sólo te queda publicarla, es decir, copiar los archivos a un servidor web, y listo, ya tienes tu versión propia de jitsimeter con lista actualizada.

## Privacidad
Jitsimeter almacena los resultados de sus evaluaciones en su propio equipo, utilizando localStorage. Esta información sólo se comparte cuando usted lo decide.

El servidor dónde jitsimeter está alojado recolecta información anonimizada de las visitas, sin dirección IP, en un archivo log standard que está disponible también para los demás administradores del mismo servidor.

La información recolectada se utiliza para comprender mejor el uso y mejorar la calidad del producto.

## Colaboración

Si gustas contar tu experiencia, proponer nuevas instancias o contactarte puedes utilizar los issues de 0xacab.org aquí: https://0xacab.org/faras/jitsimeter/issues o escrbir a faras@partidopirata.com.ar

La lista original de instancias es de [favstarmafia](https://codeberg.org/favstarmafia/Jitsi_Server_Liste/).
Gracias a las piratas de la barca de infra, a las compas de autodefensa, latam y el club de software libre. Gracias a la comunidad t.me/jitsies por el feedback.

### Traducciones
Para traducciones estamos usando [este issue](https://0xacab.org/faras/jitsimeter/issues/1).

Gracias a les traductores y revisores:
- Inglés, italiano, francés, catalán: Ona (+apertium).
- Alemán: Favstarmafia, Sofía y heroínas anónimas.
